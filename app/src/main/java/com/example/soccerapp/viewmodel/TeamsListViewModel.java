package com.example.soccerapp.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import com.example.soccerapp.model.teamlist.TeamList;
import com.example.soccerapp.repository.TeamsListRepository;


public class TeamsListViewModel extends AndroidViewModel {


    private TeamsListRepository teamsListRepository;
    private LiveData<TeamList> teamsLiveData;

    public TeamsListViewModel(@NonNull Application application) {
        super(application);
    }


    public void init() {
        teamsListRepository = new TeamsListRepository();
        teamsLiveData = teamsListRepository.getTeamsLiveData();
    }


    public void listTeams(String leagueName)  {
       teamsListRepository.listTeams(leagueName);
    }


    public LiveData<TeamList> getTeamsLiveData() {
        return teamsLiveData;
    }


}
