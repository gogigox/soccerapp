package com.example.soccerapp.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.soccerapp.model.leaguelist.ListCountries;
import com.example.soccerapp.repository.LeaguesListRepository;


public class LeaguesListViewModel extends AndroidViewModel {


    private LeaguesListRepository leaguesListRepository;
    private LiveData<ListCountries> leaguesLiveData;

    public LeaguesListViewModel(@NonNull Application application) {
        super(application);
    }

    public void init() {
        leaguesListRepository = new LeaguesListRepository();
       leaguesLiveData = leaguesListRepository.getLeaguesLiveData();
    }


    public void listLeagues(String country, String sport)  {
        leaguesListRepository.listLeagues(country, sport);
    }


    public LiveData<ListCountries> getLeaguesLiveData() {
        return leaguesLiveData;
    }



}
