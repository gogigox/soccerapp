package com.example.soccerapp.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.soccerapp.model.events.EventsList;
import com.example.soccerapp.repository.EventsRepository;


public class EventsViewModel extends AndroidViewModel {

    private EventsRepository eventsRepository;
    private LiveData<EventsList> eventsLiveData;


    public EventsViewModel(@NonNull Application application) {
        super(application);
    }


    public void init() {
        eventsRepository = new EventsRepository();
        eventsLiveData = eventsRepository.getEventsLiveData();
    }


    public void getEvents(int id)  {
        eventsRepository.getEvents(id);
    }


    public LiveData<EventsList> getEventsLiveData() {
        return eventsLiveData;
    }


}
