package com.example.soccerapp.api;



import com.example.soccerapp.model.events.EventsList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface EventService {

    @GET("eventsnext.php")
    Call<EventsList> getEvents(
            @Query("id") int id
    );
}
