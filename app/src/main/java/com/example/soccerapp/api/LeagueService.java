package com.example.soccerapp.api;

import com.example.soccerapp.model.leaguelist.ListCountries;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface LeagueService {

    @GET("search_all_leagues.php")
    Call<ListCountries> getLeagues(
            @Query("c") String country,
            @Query("s") String sport
    );
}
