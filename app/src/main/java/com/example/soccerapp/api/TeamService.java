package com.example.soccerapp.api;


import com.example.soccerapp.model.teamlist.TeamList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface TeamService {


    @GET("search_all_teams.php")
    Call<TeamList> getTeams(
            @Query("l") String leagueName

    );
}
