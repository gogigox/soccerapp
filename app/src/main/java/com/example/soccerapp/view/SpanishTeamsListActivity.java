package com.example.soccerapp.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.example.soccerapp.R;
import com.example.soccerapp.adapter.TeamsListAdapter;
import com.example.soccerapp.model.teamlist.Team;
import com.example.soccerapp.model.teamlist.TeamList;
import com.example.soccerapp.viewmodel.TeamsListViewModel;

import java.util.ArrayList;
import java.util.List;


public class SpanishTeamsListActivity extends AppCompatActivity implements TeamsListAdapter.OnRVItemClick {

    private TeamsListViewModel viewModel;
    private TeamsListAdapter adapter;
    private List<Team> teamsList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spanish_teams_list);



        adapter = new TeamsListAdapter(teamsList, this);

        viewModel = new ViewModelProvider(this).get(TeamsListViewModel.class);
        viewModel.init();
        viewModel.getTeamsLiveData().observe(this, new Observer<TeamList>() {
            @Override
            public void onChanged(TeamList teamList) {
                if (teamList != null) {
                    adapter.setResults(teamList.getTeams());
                }
            }
        });

        RecyclerView recyclerView = findViewById(R.id.rv_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(adapter);


        performSearch();

    }

    public void performSearch() {
        String leagueName = getIntent().getStringExtra("leagueName");
        viewModel.listTeams(leagueName);
    }



    @Override
    public void onRVItemclick(Team team) {
        Intent intent = new Intent(this, Next5EventsActivity.class);
        intent.putExtra("teamId", team.getIdTeam());
        intent.putExtra("teamName", team.getStrTeam());
        intent.putExtra("teamBadge", team.getStrTeamBadge());
        startActivity(intent);

    }
}