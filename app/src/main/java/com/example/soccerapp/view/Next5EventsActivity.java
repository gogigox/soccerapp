package com.example.soccerapp.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.soccerapp.R;
import com.example.soccerapp.model.events.EventsList;
import com.example.soccerapp.viewmodel.EventsViewModel;


public class Next5EventsActivity extends AppCompatActivity {

    private EventsViewModel viewModel;
    private TextView tvTeamName;
    private ImageView ivTeamBadge;
    private EditText etDate1, etDate2, etDate3, etDate4, etDate5,
            etHt1, etHt2, etHt3, etHt4, etHt5, etAt1, etAt2, etAt3, etAt4, etAt5;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next5_events);


        tvTeamName = findViewById(R.id.tv_team_name);
        ivTeamBadge = findViewById(R.id.iv_team_badge);
        etDate1 = findViewById(R.id.et_date1);
        etDate1.setFocusable(false);
        etDate2 = findViewById(R.id.et_date2);
        etDate2.setFocusable(false);
        etDate3 = findViewById(R.id.et_date3);
        etDate3.setFocusable(false);
        etDate4 = findViewById(R.id.et_date4);
        etDate4.setFocusable(false);
        etDate5 = findViewById(R.id.et_date5);
        etDate5.setFocusable(false);
        etHt1 = findViewById(R.id.et_ht1);
        etHt1.setFocusable(false);
        etHt2 = findViewById(R.id.et_ht2);
        etHt2.setFocusable(false);
        etHt3 = findViewById(R.id.et_ht3);
        etHt3.setFocusable(false);
        etHt4 = findViewById(R.id.et_ht4);
        etHt4.setFocusable(false);
        etHt5 = findViewById(R.id.et_ht5);
        etHt5.setFocusable(false);
        etAt1 = findViewById(R.id.et_at1);
        etAt1.setFocusable(false);
        etAt2 = findViewById(R.id.et_at2);
        etAt2.setFocusable(false);
        etAt3 = findViewById(R.id.et_at3);
        etAt3.setFocusable(false);
        etAt4 = findViewById(R.id.et_at4);
        etAt4.setFocusable(false);
        etAt5 = findViewById(R.id.et_at5);
        etAt5.setFocusable(false);


        setTeamData();

        viewModel = new ViewModelProvider(this).get(EventsViewModel.class);
        viewModel.init();
        viewModel.getEventsLiveData().observe(this, new Observer<EventsList>() {
            @Override
            public void onChanged(EventsList eventsList) {

                try {
                    if (eventsList != null) {

                        for (int i = 1; i <= 5; i++) {

                            int id = getResources().getIdentifier("et_date" + i, "id", getApplicationContext().getPackageName());
                            EditText etDay = findViewById(id);
                            etDay.setText(eventsList.getEvents().get(i - 1).getDateEvent());

                            int id2 = getResources().getIdentifier("et_ht" + i, "id", getApplicationContext().getPackageName());
                            EditText etHomeTeam = findViewById(id2);
                            etHomeTeam.setText(eventsList.getEvents().get(i - 1).getStrHomeTeam());

                            int id3 = getResources().getIdentifier("et_at" + i, "id", getApplicationContext().getPackageName());
                            EditText etAwayTeam = findViewById(id3);
                            etAwayTeam.setText(eventsList.getEvents().get(i - 1).getStrAwayTeam());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        performSearch();

    }


    public void performSearch() {
        String strTeamId = getIntent().getStringExtra("teamId");
        int teamId = Integer.parseInt(strTeamId);
        viewModel.getEvents(teamId);

    }


    public void setTeamData() {

        String teamName = getIntent().getStringExtra("teamName");
        String teamBadge = getIntent().getStringExtra("teamBadge");

        tvTeamName.setText(teamName);

        String imageUrl = teamBadge
                .replace("http://", "https://");

        Glide.with(getApplicationContext())
                .load(imageUrl)
                .into(ivTeamBadge);


    }


}