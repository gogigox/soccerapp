package com.example.soccerapp.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.content.Intent;
import android.os.Bundle;

import com.example.soccerapp.R;
import com.example.soccerapp.adapter.LeaguesListAdapter;
import com.example.soccerapp.model.leaguelist.Country;
import com.example.soccerapp.model.leaguelist.ListCountries;
import com.example.soccerapp.viewmodel.LeaguesListViewModel;

import java.util.ArrayList;
import java.util.List;


public class EnglishLeaguesListActivity extends AppCompatActivity implements LeaguesListAdapter.OnRVItemClick {


    private LeaguesListViewModel viewModel;
    private LeaguesListAdapter adapter;
    private List<Country> leaguesList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_english_leagues_list);

        adapter = new LeaguesListAdapter(leaguesList, this);

        viewModel = new ViewModelProvider(this).get(LeaguesListViewModel.class);
        viewModel.init();
        viewModel.getLeaguesLiveData().observe(this, new Observer<ListCountries>() {
            @Override
            public void onChanged(ListCountries listCountries) {
                if (listCountries != null) {
                    adapter.setResults(listCountries.getCountrys());
                }
            }
        });

        RecyclerView recyclerView = findViewById(R.id.rv_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(adapter);


        performSearch();

    }


    public void performSearch() {

        viewModel.listLeagues("England", "Soccer");
    }


    @Override
    public void onRVItemclick(Country country) {

        Intent intent = new Intent(this, EnglishTeamsListActivity.class);
        intent.putExtra("leagueName", country.getStrLeague());
        startActivity(intent);

    }
}