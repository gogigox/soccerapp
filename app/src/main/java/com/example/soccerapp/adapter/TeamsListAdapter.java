package com.example.soccerapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.soccerapp.R;
import com.example.soccerapp.model.teamlist.Team;

import java.util.List;


public class TeamsListAdapter extends RecyclerView.Adapter<TeamsListAdapter.MyViewHolder> {

    private List<Team> results;

    public TeamsListAdapter.OnRVItemClick listenerTeamsList;

    public interface OnRVItemClick {
        void onRVItemclick(Team team);
    }

    public TeamsListAdapter(List<Team> results, OnRVItemClick listenerTeamsList) {
        this.results = results;
        this.listenerTeamsList = listenerTeamsList;
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tvName;
        private ImageView ivTeamBadge;
        View view;

        public MyViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tvName = itemView.findViewById(R.id.tv_team_name);
            ivTeamBadge = itemView.findViewById(R.id.iv_team_badge);
        }

        public void bind(final Team team, final TeamsListAdapter.OnRVItemClick listener) {

            if (team != null) {

                tvName.setText(team.getStrTeam());

                String imageUrl = team.getStrTeamBadge()
                        .replace("http://", "https://");

                Glide.with(itemView)
                        .load(imageUrl)
                        .into(ivTeamBadge);


                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onRVItemclick(team);
                    }
                });
            }
        }
    }

    @NonNull
    @Override
    public TeamsListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.teams_list_single_item, viewGroup, false);
        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(TeamsListAdapter.MyViewHolder myViewHolder, int i) {

        myViewHolder.bind(results.get(i), listenerTeamsList);
    }


    @Override
    public int getItemCount() {
        return results.size();
    }

    public void setResults(List<Team> results) {
        this.results = results;
        notifyDataSetChanged();
    }

}
