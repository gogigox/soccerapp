package com.example.soccerapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.soccerapp.R;
import com.example.soccerapp.model.leaguelist.Country;

import java.util.List;


public class LeaguesListAdapter extends RecyclerView.Adapter<LeaguesListAdapter.MyViewHolder> {


    private List<Country> results;

    public LeaguesListAdapter.OnRVItemClick listenerLeaguesList;

    public interface OnRVItemClick {
        void onRVItemclick(Country country);
    }

    public LeaguesListAdapter(List<Country> results, OnRVItemClick listenerLeaguesList) {
        this.results = results;
        this.listenerLeaguesList = listenerLeaguesList;
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tvName;
        private ImageView ivLeagueBadge;
        View view;

        public MyViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tvName = itemView.findViewById(R.id.tv_league_name);
            ivLeagueBadge = itemView.findViewById(R.id.iv_league_badge);
        }

        public void bind(final Country country, final LeaguesListAdapter.OnRVItemClick listener) {

            if (country != null) {

                tvName.setText(country.getStrLeague());

                String imageUrl = country.getStrBadge()
                        .replace("http://", "https://");

                Glide.with(itemView)
                        .load(imageUrl)
                        .into(ivLeagueBadge);


                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onRVItemclick(country);
                    }
                });

            }

        }
    }


    @NonNull
    @Override
    public LeaguesListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.leagues_list_single_item, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LeaguesListAdapter.MyViewHolder myViewHolder, int i) {

        myViewHolder.bind(results.get(i), listenerLeaguesList);
    }


    @Override
    public int getItemCount() {
        return results.size();
    }


    public void setResults(List<Country> results) {
        this.results = results;
        notifyDataSetChanged();
    }
}
