package com.example.soccerapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.soccerapp.view.EnglishLeaguesListActivity;
import com.example.soccerapp.view.SpanishLeaguesListActivity;

public class MainActivity extends AppCompatActivity {


    private CardView cvEnglishLeagues, cvSpanishLeagues;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cvEnglishLeagues = findViewById(R.id.card_layout_england);
        cvSpanishLeagues = findViewById(R.id.card_layout_spain);

        cvEnglishLeagues.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), EnglishLeaguesListActivity.class);
                startActivity(intent);
            }
        });

        cvSpanishLeagues.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), SpanishLeaguesListActivity.class);
                startActivity(intent);
            }
        });

    }
}