package com.example.soccerapp.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;


import com.example.soccerapp.api.LeagueService;
import com.example.soccerapp.model.leaguelist.ListCountries;

import org.jetbrains.annotations.NotNull;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;


public class LeaguesListRepository {

    private static final String BASE_URL = "https://www.thesportsdb.com/api/v1/json/1/";


    private LeagueService leagueService;
    private MutableLiveData<ListCountries> leaguesLiveData;


    public LeaguesListRepository() {
        leaguesLiveData = new MutableLiveData<>();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

       leagueService = new retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(LeagueService.class);
    }

    public void listLeagues(String country, String sport) {
        leagueService.getLeagues(country, sport)
                .enqueue(new Callback<ListCountries>() {
                    @Override
                    public void onResponse(@NotNull Call<ListCountries> call, @NotNull Response<ListCountries> response) {
                        if (response.body() != null) {
                            leaguesLiveData.postValue(response.body());
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<ListCountries> call, Throwable t) {
                        leaguesLiveData.postValue(null);
                    }
                });
    }


    public LiveData<ListCountries> getLeaguesLiveData() {
        return leaguesLiveData;
    }




}
