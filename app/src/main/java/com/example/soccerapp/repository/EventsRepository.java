package com.example.soccerapp.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;


import com.example.soccerapp.api.EventService;
import com.example.soccerapp.model.events.EventsList;

import org.jetbrains.annotations.NotNull;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;


public class EventsRepository {

    private static final String BASE_URL = "https://www.thesportsdb.com/api/v1/json/1/";


    private EventService eventService;
    private MutableLiveData<EventsList> eventsLiveData;

    public EventsRepository() {
        eventsLiveData = new MutableLiveData<>();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        eventService = new retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(EventService.class);
    }

    public void getEvents(int id) {
        eventService.getEvents(id)
                .enqueue(new Callback<EventsList>() {
                    @Override
                    public void onResponse(@NotNull Call<EventsList> call, @NotNull Response<EventsList> response) {
                        if (response.body() != null) {
                            eventsLiveData.postValue(response.body());
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<EventsList> call, Throwable t) {
                        eventsLiveData.postValue(null);
                    }
                });
    }

    public LiveData<EventsList> getEventsLiveData() {
        return eventsLiveData;
    }
}
