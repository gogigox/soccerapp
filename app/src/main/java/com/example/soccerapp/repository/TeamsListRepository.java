package com.example.soccerapp.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.soccerapp.api.TeamService;
import com.example.soccerapp.model.teamlist.TeamList;

import org.jetbrains.annotations.NotNull;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;


public class TeamsListRepository {


    private static final String BASE_URL = "https://www.thesportsdb.com/api/v1/json/1/";

    private TeamService teamService;
    private MutableLiveData<TeamList> teamsLiveData;


    public TeamsListRepository() {
        teamsLiveData = new MutableLiveData<>();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        teamService = new retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(TeamService.class);
    }

    public void listTeams(String leagueName) {
        teamService.getTeams(leagueName)
                .enqueue(new Callback<TeamList>() {
                    @Override
                    public void onResponse(@NotNull Call<TeamList> call, @NotNull Response<TeamList> response) {
                        if (response.body() != null) {
                            teamsLiveData.postValue(response.body());
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<TeamList> call, Throwable t) {
                        teamsLiveData.postValue(null);
                    }
                });
    }

    public LiveData<TeamList> getTeamsLiveData() {
        return teamsLiveData;
    }




}
