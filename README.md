## **Description**

This app provides insight in English and Spanish soccer leagues. User can see all English and Spanish soccer leagues, when a league is selected all the teams that belong to it are displayed. When a team is selected, the upcoming five matches are shown. User can see the date of matches and also which is the visiting team and which is the home team.

In order to have cleaner and more extensible code, architecture of this app is built in MVVM design pattern. All live data are supplied from internet, using REST API and connection with website: https://www.thesportsdb.com/ ( Retrofit2 is used to consume REST API). For thumbnail images Glide library is used.

Guided by the principles of responsive UI design this app is customized for different screen sizes and resolutions.



## **Screenshots**

![screenshot1](https://gitlab.com/gogigox/soccerapp/-/blob/master/screenshots/Screenshot_20200913-095747.png)
<!--  -->
![screenshot2](https://gitlab.com/gogigox/soccerapp/-/blob/master/screenshots/Screenshot_20200913-095800.png)
<!--  -->
![screenshot3](https://gitlab.com/gogigox/soccerapp/-/blob/master/screenshots/Screenshot_20200913-095808.png)
<!--  -->
![screenshot4](https://gitlab.com/gogigox/soccerapp/-/blob/master/screenshots/Screenshot_20200913-095948.png)
